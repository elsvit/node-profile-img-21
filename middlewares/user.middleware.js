const { userService } = require('../services');
const { ErrorHandler, errorMessage, errorStatus } = require('../errors');

module.exports = {
  checkUserEmailUnique: async (req, res, next) => {
    try {
      const { email } = req.body;

      const userByEmail = await userService.getUserByEmail(email.trim());

      if (userByEmail) {
        throw new ErrorHandler(errorStatus.CONFLICT, errorMessage.EMAIL_EXISTS);
      }

      next();
    } catch (err) {
      next(err);
    }
  },

  checkUserIdExists: async (req, res, next) => {
    try {
      const user = await userService.getUserById(req.params.user_id);

      if (!user) {
        throw new ErrorHandler(errorStatus.NOT_FOUND, errorMessage.NOT_FOUND);
      }

      req.locals = { user };

      next();
    } catch (err) {
      next(err);
    }
  }
};
