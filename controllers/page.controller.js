const staticPages = require('../constants/staticPages.enam');
const { errorStatus, errorMessage } = require('../errors');
const { getUserById, getAllUsers } = require('../services/user.service');

module.exports = {
  newUser: (req, res) => res.render(staticPages.userForm, { formType: 'New', formAction: 'api/users' }),

  editUser: async (req, res) => {
    const userId = req.params.user_id;

    try {
      const user = await getUserById(userId);

      const renderData = {
        formType: 'Edit',
        formAction: `api/users/update/${userId}`,
        id: userId,
        name: user.name,
        surname: user.name,
        email: user.email,
        img: `/${userId}.jpg`,
      };

      return res.render(staticPages.userForm, renderData);
    } catch (err) {
      return res.status(errorStatus.BAD_REQUEST).json({ message: errorMessage.SOMETHING_WRONG });
    }
  },

  users: async (req, res) => {
    try {
      const users = await getAllUsers();

      const normalizedUsers = users.map((user) => {
        const userObj = user.toObject();
        return ({ ...userObj, id: user._id.toString() });
      });

      return res.render(staticPages.users, { users: normalizedUsers });
    } catch (err) {
      return res.status(errorStatus.BAD_REQUEST).json({ message: errorMessage.SOMETHING_WRONG });
    }
  },
};
