const router = require('express').Router();

const { pageController } = require('../controllers');
const pages = require('../constants/pages.enam');

router.get(`/${pages.users}`, pageController.users);

router.get(`/${pages.users}/new`, pageController.newUser);

router.get(`/${pages.users}/edit/:user_id`, pageController.editUser);

module.exports = router;
