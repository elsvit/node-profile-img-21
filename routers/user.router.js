const router = require('express').Router();

const { userController } = require('../controllers');
const { checkUserIdExists, checkUserEmailUnique } = require('../middlewares/user.middleware');

router.post('/', checkUserEmailUnique, userController.createUser);

router.get('/', userController.getAllUsers);

router.get('/:user_id', checkUserIdExists, userController.getUserById);

router.put('/:user_id', checkUserIdExists, userController.updateUserById);

router.delete('/:user_id', checkUserIdExists, userController.deleteUserById);

module.exports = router;
