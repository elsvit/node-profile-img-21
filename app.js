const express = require('express');
const mongoose = require('mongoose');
const expressHbs = require('express-handlebars');
const path = require('path');
const multer = require('multer');
const gm = require('gm');
const fs = require('fs');

const { userRouter, pageRouter } = require('./routers');
const { PORT } = require('./constants/config');
const pages = require('./constants/pages.enam');
const routes = require('./constants/routs.enam');
const { errorStatus } = require('./errors');

mongoose.connect('mongodb://localhost:27017/profileImages21');

const app = express();
const staticPath = path.join(__dirname, 'static');
const imgsPath = path.join(__dirname, 'images');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(staticPath));
app.set('view engine', '.hbs');
app.engine('.hbs', expressHbs({ defaultLayout: false }));
app.set('views', staticPath);
app.use(express.static('images'));

app.use(`/api/${routes.users}`, userRouter);

app.use('/page', pageRouter);

app.use(_mainErrorHandler);

app.get('/', (req, res) => {
  res.redirect(`/page/${pages.users}/new`);
});

// img
const handleError = (err, res) => {
  res
    .status(500)
    .contentType('text/plain')
    .end('Oops! Something went wrong!');
};

const upload = multer({
  dest: imgsPath
});

app.post(
  '/upload/:userId',
  upload.single('file'),
  (req, res) => {
    if (req.file) {
      const tempPath = req.file.path;
      const { userId } = req.params;
      const fileExt = path.extname(req.file.originalname).toLowerCase();
      if (['.jpg'].includes(fileExt)) {
      // if (['.jpeg', '.jpg', '.png'].includes(fileExt)) {
        const targetPath = path.join(imgsPath, `${userId}${fileExt}`);
        fs.rename(tempPath, targetPath, (err) => {
          console.log('Error:', err.message);
        });
        gm(targetPath)
          .resize(200, 200)
          .write(targetPath, (err) => {
            console.log('Error:', err.message);
          });
        res.redirect(`/page/${pages.users}/edit/${userId}`);
      } else {
        fs.unlink(tempPath, (err) => {
          if (err) return handleError(err, res);
          res
            .status(errorStatus.CONFLICT)
            .contentType('text/plain')
            .end('Only .jpg files are allowed!');
        });
      }
    }
  }
);

app.listen(PORT, () => {
  console.log('App listen 5000');
});

// eslint-disable-next-line no-unused-vars
function _mainErrorHandler(err, req, res, next) {
  res
    .status(err.status || 500)
    .json({ message: err.message });
}
