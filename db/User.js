const { Schema, model } = require('mongoose');

const userSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  surname: {
    type: String,
    required: false,
    trim: true
  },
  email: {
    type: String,
    required: true,
    trim: true,
    unique: true
  },
}, { timestamps: true });

const UserModel = model('user', userSchema);
UserModel.aggregate([
  { $match: { user: 1 } },
  { $project: { _id: { $toString: '$_id' } } }
]);

module.exports = UserModel;
// module.exports = model('user', userSchema);
